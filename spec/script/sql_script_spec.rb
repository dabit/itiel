require 'spec_helper'

describe Itiel::Script::SQLScript do
  before :each do
    @data_stream = { "c" => "v" }
  end

  context "with a string" do
    before :each do
      @sql_script = Itiel::Script::SQLScript.new("DELETE FROM orders")
    end

    describe :execute do
      before :each do
        @connection = double
        @sql_script.connection = :test
        allow(Itiel::Script::SQLScript).to receive(:sequel_connection).with(:test).and_return @connection
      end

      it "Executes the specified SQL script with the given connection" do
        expect(@connection).to receive(:<<).with("DELETE FROM orders")
        @sql_script.execute
      end
    end
  end

  context "with a block" do
    before :each do
      @sql_script = Itiel::Script::SQLScript.new do |row|
        "DELETE FROM orders WHERE id = #{row[:id]}"
      end
    end

    describe :execute do
      before :each do
        @connection = double
        @sql_script.connection = :test
        allow(Itiel::Script::SQLScript).to receive(:sequel_connection).with(:test).and_return @connection
      end

      it "Executes the specified SQL script with the given connection" do
        expect(@connection).to receive(:<<).with("DELETE FROM orders WHERE id = 1")
        @sql_script.execute([{ id: 1 }])
      end
    end
  end
end
