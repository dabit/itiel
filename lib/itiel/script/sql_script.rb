module Itiel
  module Script
    #
    # Executes a SQL script or command on the specified
    # connection
    #
    class SQLScript
      include ChainedStep
      include Itiel::DB::SQLConnectable

      attr_accessor :sql

      def initialize(sql=nil, &block)
        if block_given?
          self.sql = block
        else
          self.sql = sql
        end
      end

      def execute(input_stream=nil)
        db = self.class.sequel_connection(connection)
        if sql.respond_to?(:call)
          input_stream.each do |row|
            db << sql.call(row)
          end
        else
          db << sql
        end
      end
    end
  end
end
